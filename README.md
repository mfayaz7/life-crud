# lara-admin



## Installation

- clone `git@gitlab.com:mfayaz7/lara-admin.git`
- Edit `.env` and set your database connection details
- (When installed via git clone or download, run `composer update` and `php artisan key:generate` )
- `php artisan migrate`
- `npm install`
- `php artisan passport:install`
- `php artisan serve`
- `php artisan migrate --seed`

## Usage

#### Development

```bash
# Build and watch
npm run watch

```

#### Production

```bash
npm run production
```

####login
```bash
email:webmaster@admin.com

password:123456
```

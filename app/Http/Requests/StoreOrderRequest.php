<?php
namespace App\Http\Requests;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    public function authorize(){
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Order::$rules;
    }



    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return Order::$messages;
    }
}

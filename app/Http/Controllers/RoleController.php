<?php

namespace App\Http\Controllers;

use App\Models\Role as RoleModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;


class RoleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        Log::info('index role');
        $query = Role::query();
        if ($request->has('page')) {
            $roles = $query->orderBy('id', 'ASC')->paginate(5);
        }else{
            Log::info('ger roles');
            $roles = $query->orderBy('id', 'ASC')->get();
        }
        return response()->json( $roles,200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
        ]);

        $role = Role::create(['name' => $request->input('name')]);
        return response()->json( $role,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Role $role)
    {
        Log::info('delete');
        if($role->delete()){
            return response()->json([
                'message' => 'User delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

class ProductController extends Controller
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index(Request $request)
    {
        $products = Product::query()->paginate(5);
        return response()->json( $products,200);
    }

    public function store(StoreProductRequest $request){
        Log::info('store product');
        $product = $this->productRepository->create($request);
        return response()->json(['product'=>$product],201);
    }

    public function update(Product $product,UpdateProductRequest $request){
        $product = $this->productRepository->update($product,$request);
        return $product;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product)
    {
        Log::info('delete');
        if($product->delete()){
            return response()->json([
                'message' => 'Product delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}

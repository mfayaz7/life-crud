<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }


    public function index(Request $request)
    {
        $query=User::with('roles');
        if ($request->has('search')) {
            $query->where('name', 'like', '%' . $request->get('search') . '%')
                ->orWhere('email', 'like', '%' . $request->get('search') . '%');
        }
        if ($request->has('filter')) {
            $query->whereHas('roles', function ($query) use ($request) {
                return $query->where('name', $request->get('filter'));
            });
        }
            $users = $query->orderBy('id' ,'ASC' )->paginate(5);
        return response()->json( $users,200);
    }


    public function store(StoreUserRequest $request){
        Log::info('store');
        $user = $this->userRepository->create($request);
        return response()->json(['user'=>$user],201);
    }

    public function update(User $product,UpdateUserRequest $request){
        $product = $this->userRepository->update($product,$request);
        return response()->json(['product'=>$product],201);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        Log::info('delete');
        if($user->delete()){
            return response()->json([
                'message' => 'User delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}

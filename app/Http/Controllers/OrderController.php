<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

class OrderController extends Controller
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function index(Request $request)
    {
        $orders = Order::query()->with(['user','product'])->paginate(5);
        return response()->json( $orders,200);
    }

    public function store(StoreOrderRequest $request){
        Log::info('store order');
        $order = $this->orderRepository->create($request);
        return response()->json(['order'=>$order],201);
    }

    public function update(Order $order,UpdateOrderRequest $request){
        $order = $this->orderRepository->update($order,$request);
        return $order;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Order $order)
    {
        Log::info('delete');
        if($order->delete()){
            return response()->json([
                'message' => 'Order delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>  $this->id,
            'order_id' =>  $this->order_id,
            'product_id' =>  $this->product_id,
            'order_name' =>  $this->order->name,
            'product_name' =>  $this->product->name,
            'price' => $this->price,
            'created_at' => (string) $this->created_at,
            'deleted_at' => (string) $this->deleted_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}

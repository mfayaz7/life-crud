<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name'=>'required',
        'price'=>'required',
    ];

    /**
     * Validation Messages
     *
     * @var array
     */
    public static $messages = [
        'name.required' => 'Name required',
        'price.required' => 'Price required',
    ];

}

<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'product_id',
        'price',
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id'=>'required',
        'product_id'=>'required',
        'price'=>'required',
    ];

    /**
     * Validation Messages
     *
     * @var array
     */
    public static $messages = [
        'user_id.required' => 'User required',
        'product_id.required' => 'Product required',
        'price.required' => 'Price required',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}

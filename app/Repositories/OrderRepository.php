<?php
namespace App\Repositories;

use App\Http\Resources\ProductResource;
use App\Models\Order;
use App\Models\Product;

class OrderRepository{

    public function create($orderData){
        $order = new Order();
        $orderId=Order::query()->find($orderData->order_id);
        $productId=Product::query()->find($orderData->product_id);
        $order->order_id = $orderId;
        $order->product_id = $productId;
        $order->price = $orderData->price;
        $order->save();

        return $order;
    }

    public function update($order,$orderData){
        $orderId=Order::query()->find($orderData->order_id);
        $productId=Product::query()->find($orderData->product_id);
        $order->order_id = $orderId;
        $order->product_id = $productId;
        $order->price= $orderData->price;
        $order->save();
        return new OrderResource($order);
    }


}

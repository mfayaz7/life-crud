<?php
namespace App\Repositories;

use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductRepository{

    public function create($productData){
        $product = new Product();
        $product->name = $productData->name;
        $product->price = $productData->price;
        $product->save();

        return $product;
    }

    public function update($product,$productData){
        $product->name =  $productData->name;
        $product->price= $productData->price;
        $product->save();
        return new ProductResource($product);
    }


}

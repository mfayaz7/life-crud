<?php
namespace App\Repositories;

use App\Models\User;

class UserRepository{

    public function create($userData){
        $user = new User();
        $user->name = $userData->name;
        $user->email = $userData->email;
        $user->password = bcrypt($userData->password);
        $user->save();

        return $user;
    }


    public function update($user,$userData){
        $user->name = $userData->name;
        $user->email = $userData->email;
        $user->save();
        return $user;
    }

}

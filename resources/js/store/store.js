import Vue from 'vue';
import Vuex from 'vuex';

import * as auth from "../services/auth_service";

Vue.use(Vuex);

export default new Vuex.Store({
    state:{
        isLoggedIn:false,
        apiURL:'http://127.0.0.1:8000/api',
        serverPath:'http://127.0.0.1:8000',
        profile:localStorage.getItem('profile'),
        role:localStorage.getItem('role')
    },

    getters: {
        profile: state => {
            return state.profile;
        },
        role: state => {
            return state.role;
        },
    },

    mutations: {
        authenticate(state,payload){
            state.isLoggedIn = auth.isLoggedIn();
            if (state.isLoggedIn){
                state.profile = payload;
                state.role=payload.roles[0].name;
                localStorage.setItem('profile', payload);
                localStorage.setItem('role', payload.roles[0].name);

            }else {
                state.profile = {};
                state.role=null;
            }
        },
    },
    actions: {
        authenticate(context,payload){
            context.commit('authenticate',payload)
        }
    },
})

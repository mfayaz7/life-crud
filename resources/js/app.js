import Vue from 'vue';
import App from  './App.vue';
import router from './router';
import store from './store/store';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import 'bootstrap-vue/dist/bootstrap-vue.css'
import FlashMessage from '@smartweb/vue-flash-message';
import VueConfirmDialog from 'vue-confirm-dialog';

Vue.component('pagination', require('laravel-vue-pagination'));


Vue.use(VueConfirmDialog);
Vue.component('vue-confirm-dialog', VueConfirmDialog.default);
Vue.use(FlashMessage);
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

const app= new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});

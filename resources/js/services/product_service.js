import {http} from './http_service'

export function createProduct(data) {
    return http().post('/products',data);
}

export function deleteProduct(id) {
    return http().delete(`/products/${id}`);
}

export function updateProduct(id,data) {
    return http().post(`/products/${id}`,data);
}

export function getProduct(params) {
    return http().get('/products', {params: params});
}

<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $superAdminRole = Role::whereName('Super Admin')->first();
        $adminRole = Role::whereName('Admin')->first();
        $userRole = Role::whereName('User')->first();

        // Seed test admin
        $seededAdminEmail = 'webmaster@admin.com';
        $user = User::where('email', '=', $seededAdminEmail)->first();
        if ($user === null) {
            $user = User::create([
                'name' => 'superadmin',
                'email' => 'webmaster@admin.com',
                'password' =>bcrypt('123456'),
            ]);
            $user->assignRole($superAdminRole);
            $user->save();
        }

        $adminUser = User::where('email', '=', 'test_admin@admin.com')->first();
        if ($adminUser === null) {
            $adminUser = User::create([
                'name' => 'admin',
                'email' => 'test_admin@admin.com',
                'password' => bcrypt('123456'),
            ]);
            $adminUser->assignRole($adminRole);
            $adminUser->save();
        }



        // Seed test user
        $user = User::where('email', '=', 'testuser@admin.com')->first();
        if ($user === null) {
            $user = User::create([
                'name' => 'testuser',
                'email' => 'testuser@admin.com',
                'password' => bcrypt('123456'),
            ]);
            $user->assignRole($userRole);
            $user->save();
        }
    }
}
